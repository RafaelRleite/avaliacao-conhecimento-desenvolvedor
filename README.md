# Sistema de Gerenciamento de Colaboradores

| É um sistema de gerenciamento de colaboradores desenvolvido em Java, que utiliza a arquitetura de microserviços com Spring e o banco Postgresql. Ele possui uma interface de usuário web utilizando HTML5, CSS3 e Angular. Além disso, o projeto é gerenciado pelo GIT e hospedado no GitLab. O objetivo do Gerenciador de Colaboradores é ajudar as empresas a gerenciar seus funcionários de forma mais eficiente, permitindo que eles tenham uma visão clara de relacionamentos entre gerentes e subordinados.|
| --- |


## Entidades
    * Colaboradores(Abstrata)
    * Presidente
    * Gerente
    * Subordinado

## Funcionalidades
    * Cadastro
    * Alteração
    * Delete
    * Inclusão de Colaborador em lista
    * Exclusão de Colaborador em lista
    * Filtro de Colaboradores por ano de Admissão
    * Utilização de HTML5, CSS3 e Angular para a interface do usuário
    * Controle de versão utilizando GIT e GitHub para gerenciamento de projetos  
---
## Tecnologias utilizadas
    * Java
    * Spring
    * Postgresql
    * HTML5
    * CSS3
    * Angular
    * GIT
    * GitLab
---
## Como executar
Para executar o sistema, é necessário ter instalado as seguintes tecnologias:

- Backend: **[Java](https://openjdk.java.net/install/)**
- Gerenciador de Depêndencias: **[Maven](https://maven.apache.org/download.cgi)**
- Frontend: **[Node](https://nodejs.org/en/download)**

Com as tecnologias instaladas, siga os seguintes passos:

---
## Clone o repositório do GitHub:

```
git clone https://gitlab.com/RafaelRleite/avaliacao-conhecimento-desenvolvedor.git
```

## Navegue para o diretório do projeto:

```
cd avaliacao-conhecimento-desenvolvedor
```

## Instale o ClIN do Angular:

```
npm install -g @angular/cli
```

## Construa o projeto com o Maven:

```
mvn clean install
```

## Suba o servidor Spring:

```
mvn spring-boot:run
```

## Suba o servidor Angular:

```
ng serve
```

## Acesse o sistema no navegador:

```
http://localhost:4200
```

---

    
