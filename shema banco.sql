CREATE TABLE public.colaborador (
	id int8 NOT NULL,
	cpf varchar(255) NOT NULL,
	data_admissao date NOT NULL,
	funcao varchar(255) NOT NULL,
	nome varchar(255) NOT NULL,
	nova_funcao varchar(255) NULL,
	remuneracao numeric(38, 2) NOT NULL,
	CONSTRAINT colaborador_pkey PRIMARY KEY (id),
	CONSTRAINT colaborador_ukey UNIQUE (cpf),
	CONSTRAINT uk_dluwox59xryije1xr98mqc60n UNIQUE (cpf)
);

INSERT INTO public.colaborador
(id, cpf, data_admissao, funcao, nome, nova_funcao, remuneracao) VALUES
(1, '02213954151', '2023-01-01', 'Gerente', 'Rafael Rodrigues Leite','', 1000.85),
(2, '98765432101', '2023-02-15', 'Subordinado', 'Maria Silva', '', 2000.00),
(3, '12345678902', '2023-03-10', 'Subordinado', 'João Santos', '', 1500.99),
(4, '98765432103', '2023-04-20', 'Gerente', 'Ana Lima', '', 1800.00),
(5, '12345678904', '2023-05-05', 'Subordinado', 'Pedro Castro', '', 2201.50),
(6, '98765432105', '2023-06-15', 'Gerente', 'Julia Santos','', 1700.78),
(7, '12345678906', '2023-07-20', 'Subordinado',  'Lucas Oliveira', '', 1900.00),
(8, '98765432107', '2023-08-10', 'Subordinado',  'Carolina Fernandes', '', 2100.00),
(9, '12345678908', '2023-09-25', 'Gerente',  'Luiza Costa','', 1600.99),
(10, '98765432109', '2023-01-01', 'Presidente',  'Gustavo Silva', '', 2300.56),
(11, '12345678910', '2023-02-15', 'Subordinado', 'Fernanda Sousa', '', 2500.00),
(12, '98765432111', '2023-03-10', 'Gerente', 'Rodrigo Mendes', '', 2800.00),
(13, '12345678912', '2024-04-20', 'Subordinado', 'Mariana Almeida', '', 2300.00),
(14, '98765432113', '2024-05-05', 'Subordinado', 'Fábio Gomes', '', 2750.37),
(15, '12345678914', '2024-06-15', 'Gerente', 'Patrícia Lima', '', 3300.99),
(16, '98765432115', '2024-07-20', 'Subordinado', 'Ricardo Martins', '', 2400.00),
(17, '12345678916', '2024-08-10', 'Subordinado', 'Isabela Castro', '', 2600.00),
(18, '98765432117', '2024-09-25', 'Gerente', 'Guilherme Santos', '', 25000.00),
(19, '12345678918', '2025-01-01', 'Subordinado', 'Camila Fernandes', '', 2900.00),
(20, '98765432119', '2025-02-15', 'Gerente', 'Eduardo Lima', '', 2700.44),
(21, '12345678920', '2025-03-10', 'Subordinado', 'Mariana Silva', '', 400.00),
(22, '98765432121', '2025-04-20', 'Gerente', 'Rafaela Oliveira', '', 2800.00),
(23, '12345678922', '2025-05-05', 'Subordinado', 'Lucas Santos', '', 2600.00),
(24, '98765432123', '2025-06-15', 'Gerente', 'Fernanda Lima', '', 3000.00),
(25, '12345678924', '2025-07-20', 'Subordinado', 'Gustavo Rodrigues', '', 2800.00),
(26, '98765432125', '2025-08-10', 'Gerente', 'Carolina Castro', '', 3200.00),
(27, '12345678926', '2025-09-25', 'Subordinado', 'Eduardo Silva', '', 3000.00),
(28, '98765432127', '2026-01-01', 'Presidente', 'Mariana Santos', '', 3500.00),
(29, '12345678928', '2026-02-15', 'Subordinado', 'Ricardo Lima', '', 3200.00),
(30, '98765432129', '2026-03-10', 'Gerente', 'Patrícia Oliveira', '', 3600.00);


CREATE TABLE public.presidente (
	colaborador int8 NOT NULL,
	CONSTRAINT presidente_pkey PRIMARY KEY (colaborador),
	CONSTRAINT fk_colaborador_id FOREIGN KEY (colaborador) REFERENCES public.colaborador(id)
);

INSERT INTO public.presidente
(colaborador)VALUES(10),(28);

CREATE TABLE public.gerente (
	colaborador int8 NOT NULL,
	presidente int8 NULL,
	CONSTRAINT gerente_pkey PRIMARY KEY (colaborador),
	CONSTRAINT fk_colaborador_id FOREIGN KEY (colaborador) REFERENCES public.colaborador(id),
	CONSTRAINT fk_presidente_id FOREIGN KEY (presidente) REFERENCES public.presidente(colaborador)
);

INSERT INTO public.gerente
(colaborador, presidente) VALUES
(1, NULL),
(4, NULL),
(6, NULL),
(9, NULL),
(12, NULL),
(15, NULL),
(18, NULL),
(20, NULL),
(22, NULL),
(24, NULL),
(26, NULL),
(30, NULL);

CREATE TABLE public.subordinado (
	colaborador int8 NOT NULL,
	gerente int8 NULL,
	presidente int8 NULL,
	CONSTRAINT subordinado_pkey PRIMARY KEY (colaborador),
	CONSTRAINT fk_colaborador_id FOREIGN KEY (colaborador) REFERENCES public.colaborador(id),
	CONSTRAINT fk_gerente_id FOREIGN KEY (gerente) REFERENCES public.gerente(colaborador),
	CONSTRAINT fk_presidente_id FOREIGN KEY (presidente) REFERENCES public.presidente(colaborador)
);

INSERT INTO public.subordinado
(colaborador, gerente, presidente) VALUES
(2, NULL, NULL),
(3, NULL, NULL),
(5, NULL, NULL),
(7, NULL, NULL),
(8, NULL, NULL),
(11, NULL,NULL),
(13, NULL,NULL),
(14, NULL,NULL),
(16, NULL,NULL),
(17, NULL,NULL),
(19, NULL,NULL),
(21, NULL,NULL),
(23, NULL,NULL),
(25, NULL,NULL),
(27, NULL,NULL),
(29, NULL,NULL);

DROP SEQUENCE public.colaborador_id_seg;

CREATE SEQUENCE public.colaborador_id_seg
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 31
	CACHE 1
	NO CYCLE;

