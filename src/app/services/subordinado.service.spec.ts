import { TestBed } from '@angular/core/testing';

import { SubordinadoService } from './subordinado.service';

describe('SubordinadoService', () => {
  let service: SubordinadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubordinadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
