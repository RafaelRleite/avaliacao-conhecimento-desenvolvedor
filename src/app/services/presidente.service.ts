import { Subordinado } from 'src/app/model/subordinado';
import { Colaborador } from 'src/app/model/colaborador';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Presidente } from '../model/presidente';
import { Page } from '../model/page';
import { Gerente } from '../model/gerente';

@Injectable({
  providedIn: 'root'
})
export class PresidenteService {

  //private API = 'http://18.233.165.193:8080/api/v1';
  private API = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) { }

  getAllList(page: number, size: number): Observable<Page> {
    return this.http.get<Page>(`${this.API}/presidentes?page=${page}&size=${size}`);
  }

  createPresidente(presidente: Colaborador): Observable<Presidente> {
    return this.http.post<Presidente>(`${this.API}/salvar-presidente`, presidente);
  }

  getPresidenteById(id: number): Observable<Presidente> {
    return this.http.get<Presidente>(`${this.API}/presidente/${id}`);
  }

  getSubordinadosPresidenteById(id: number): Observable<Subordinado[]> {
    return this.http.get<Subordinado[]>(`${this.API}/${id}/subordinados`);
  }

  getGerentesPresidenteById(id: number): Observable<Gerente[]> {
    return this.http.get<Gerente[]>(`${this.API}/${id}/presidente/gerentes`);
  }

  updatePresidente(id: number, presidente: Colaborador): Observable<Presidente> {
    return this.http.put<Presidente>(`${this.API}/alterar-presidente/${id}`, presidente);
  }

  deletePresidente(id: number): Observable<Object> {
    return this.http.delete(`${this.API}/delete-presidente/${id}`);
  }

  removeSubordinadoByid(id: number, subordinadoId: number): Observable<Presidente> {
    return this.http.get<Presidente>(`${this.API}/${id}/presidente/remover-subordinado/${subordinadoId}`)
  }

  removeGerenteByid(id: number, gerenteId: number): Observable<Presidente> {
    return this.http.get<Presidente>(`${this.API}/${id}/presidente/remover-gerente/${gerenteId}`)
  }

  adicionarGerente(id:number, gerentes: Gerente[]): Observable<Gerente[]>{
    return this.http.put<Gerente[]>(`${this.API}/${id}/presidente/adicionar-gerentes`, gerentes);
  }

  adicionarSubordinados(id:number, subordinados:Subordinado[]): Observable<Subordinado[]>{
    return this.http.put<Subordinado[]>(`${this.API}/${id}/presidente/adicionar-subordinados`, subordinados);
  }
}
