import { Subordinado } from 'src/app/model/subordinado';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Gerente } from '../model/gerente';
import { Colaborador } from '../model/colaborador';
import { Page } from '../model/page';

@Injectable({
  providedIn: 'root'
})
export class GerenteService {

  //private API = 'http://18.233.165.193:8080/api/v1';
  private API = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) { }

  getAllList(page: number, size: number): Observable<Page> {
    return this.http.get<Page>(`${this.API}/gerentes?page=${page}&size=${size}`);
  }

  geGerentes():Observable<Gerente[]>{
    return this.http.get<Gerente[]>(`${this.API}/gerentesAll`)
  }

  getGerenteById(id: number): Observable<Gerente> {
    return this.http.get<Gerente>(`${this.API}/gerente/${id}`);
  }

  getSubordinadosGerenteById(id: number): Observable<Subordinado[]> {
    return this.http.get<Subordinado[]>(`${this.API}/${id}/gerente/subordinados`);
  }

  createGerente(gerente: Colaborador): Observable<Gerente> {
    return this.http.post<Gerente>(`${this.API}/salvar-gerente`, gerente);
  }

  updateGerente(id: number, gerente: Colaborador): Observable<Gerente> {
    return this.http.put<Gerente>(`${this.API}/alterar-gerente/${id}`, gerente);
  }

  deleteGerente(id: number): Observable<Object> {
    return this.http.delete(`${this.API}/delete-gerente/${id}`);
  }

  removeSubordinadoByid(id: number, subordinadoId: number): Observable<Gerente> {
    return this.http.get<Gerente>(`${this.API}/${id}/gerente/remover-subordinado/${subordinadoId}`);
  }

  adicionarSubordinados(id:number, subordinados:Subordinado[]): Observable<Subordinado[]>{
    return this.http.put<Subordinado[]>(`${this.API}/${id}/gerente/adicionar-subordinados`, subordinados);
  }
}
