import { Subordinado } from './../model/subordinado';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Colaborador } from '../model/colaborador';
import { Page } from '../model/page';
import { Presidente } from '../model/presidente';
import { Gerente } from '../model/gerente';

@Injectable({
  providedIn: 'root'
})
export class SubordinadoService {

  //private API = 'http://18.233.165.193:8080/api/v1';
  private API = 'http://localhost:8080/api/v1';
  
  constructor(private http: HttpClient) { }

  getAllList(page: number, size: number): Observable<Page> {
    return this.http.get<Page>(`${this.API}/subordinados?page=${page}&size=${size}`);
  }

  geSubordinados():Observable<Subordinado[]>{
    return this.http.get<Subordinado[]>(`${this.API}/subornidadosAll`)
  }

  createSubordinado(subordinado: Colaborador): Observable<Subordinado>{
    return this.http.post<Subordinado>(`${this.API}/salvar-subordinado`, subordinado);
  }

  getSubordinadoById(id: number): Observable<Subordinado>{
    return this.http.get<Subordinado>(`${this.API}/subordinado/${id}`);
  }

  getPresidenteSubordinadoById(id: number) {
    return this.http.get<Presidente>(`${this.API}/${id}/subordinado/presidente`)
  }

  getGerenteSubordinadoById(id:number){
    return this.http.get<Gerente>(`${this.API}/${id}/subordinado/gerente`)
  }

  updateSubordinado(id: number, subordinado: Colaborador): Observable<Subordinado>{
    return this.http.put<Subordinado>(`${this.API}/alterar-subordinado/${id}`, subordinado);
  }

  deleteSubordinado(id: number): Observable<Object>{
    return this.http.delete(`${this.API}/delete-subordinado/${id}`);
  }
}
