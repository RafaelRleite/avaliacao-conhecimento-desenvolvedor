import { Colaborador } from 'src/app/model/colaborador';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Page } from '../model/page';

@Injectable({
  providedIn: 'root'
})
export class ColaboradorService {

  //private API = 'http://18.233.165.193:8080/api/v1';
  private API = 'http://localhost:8080/api/v1';

  constructor(private http: HttpClient) { }

  getAllList(page: number, size: number): Observable<Page> {
    return this.http.get<Page>(`${this.API}/home?page=${page}&size=${size}`);
  }

  getFilterYear(ano: number, page: number, size: number): Observable<Page> {
    return this.http.get<Page>(`${this.API}/filtrar/${ano}?page=${page}&size=${size}`);
  }

  create(colaborador: Colaborador): Observable<Colaborador> {
    return this.http.post<Colaborador>(`${this.API}/salvar-colaborador`, colaborador);
  }

  getById(id: number): Observable<Colaborador> {
    return this.http.get<Colaborador>(`${this.API}/colaborador/${id}`);
  }

  update(id: number, colaborador: Colaborador): Observable<Colaborador> {
    return this.http.put<Colaborador>(`${this.API}/alterar-colaborador/${id}`, colaborador);
  }

  delete(id: number): Observable<Colaborador> {
    return this.http.delete<Colaborador>(`${this.API}/delete-colaborador/${id}`);
  }
}
