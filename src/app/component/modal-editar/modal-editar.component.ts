import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Colaborador } from 'src/app/model/colaborador';
import { Gerente } from 'src/app/model/gerente';
import { Presidente } from 'src/app/model/presidente';
import { Subordinado } from 'src/app/model/subordinado';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { GerenteService } from 'src/app/services/gerente.service';
import { PresidenteService } from 'src/app/services/presidente.service';
import { SubordinadoService } from 'src/app/services/subordinado.service';

@Component({
  selector: 'app-modal-editar',
  templateUrl: './modal-editar.component.html',
  styleUrls: ['./modal-editar.component.css']
})
export class ModalEditarComponent implements OnInit {

  id!: number;
  colaboradorEditar: Colaborador | Gerente | Subordinado | Presidente = new Gerente;

  constructor(private service: ColaboradorService, private serviceGerente: GerenteService, private serviceSubordinado: SubordinadoService,
    private servicePresidente: PresidenteService, public modal: NgbActiveModal) { }

  ngOnInit(): void {
    this.getById();
  }

  getById() {
    this.service.getById(this.id).subscribe(data => {
      this.colaboradorEditar = data;
    })
  }

  salvar() {
    if (this.colaboradorEditar.funcao.match("Gerente")) {
      this.serviceGerente.updateGerente(this.id, this.colaboradorEditar).subscribe(data => {
        this.colaboradorEditar = data;
      });
    } else if (this.colaboradorEditar.funcao.match("Subordinado")) {
      this.serviceSubordinado.updateSubordinado(this.id, this.colaboradorEditar).subscribe(data => {
        this.colaboradorEditar = data;
      });
    } else if (this.colaboradorEditar.funcao.match("Presidente")) {
      this.servicePresidente.updatePresidente(this.id, this.colaboradorEditar).subscribe(data => {
        this.colaboradorEditar = data;
      });
    }
    this.recarregaList();
  }

  recarregaList() {
    this.modal.close();
    window.location.reload();
  }
}