import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Colaborador } from 'src/app/model/colaborador';
import { Gerente } from 'src/app/model/gerente';
import { Presidente } from 'src/app/model/presidente';
import { Subordinado } from 'src/app/model/subordinado';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { GerenteService } from 'src/app/services/gerente.service';
import { PresidenteService } from 'src/app/services/presidente.service';

@Component({
  selector: 'app-modal-remover',
  templateUrl: './modal-remover.component.html',
  styleUrls: ['./modal-remover.component.css']
})
export class ModalRemoverComponent implements OnInit {
  idRemover!: number;
  idRemovedor!: number;
  colaboradorRemovedor: Colaborador | Gerente | Presidente = new Gerente;
  colaboradorRemover: Colaborador | Subordinado | Gerente = new Gerente;

  constructor(private service: ColaboradorService, private serviceGerente: GerenteService, private servicePresidente: PresidenteService,
    public modal: NgbActiveModal) { }

  ngOnInit(): void {
    this.getById();
  }

  remover() {
    if (this.colaboradorRemovedor.funcao.match("Gerente")) {
      this.serviceGerente.removeSubordinadoByid(this.idRemovedor, this.idRemover).subscribe(data => {
        this.colaboradorRemovedor = data;
      });
    }else if (this.colaboradorRemovedor.funcao.match("Presidente")) {
      if (this.colaboradorRemover.funcao.match("Gerente")) {
        this.servicePresidente.removeGerenteByid(this.idRemovedor, this.idRemover).subscribe(data => {
          this.colaboradorRemovedor = data;
        })
      } else if (this.colaboradorRemover.funcao.match("Subordinado")) {
        this.servicePresidente.removeSubordinadoByid(this.idRemovedor, this.idRemover).subscribe(data => {
          this.colaboradorRemovedor = data;
        });
      }
    }
    this.recarregaList()
  }

  getById() {
    this.service.getById(this.idRemovedor).subscribe(data => {
      this.colaboradorRemovedor = data;
    });
    this.service.getById(this.idRemover).subscribe(data => {
      this.colaboradorRemover = data;
    });
  }

  recarregaList() {
    this.modal.close();
    window.location.reload();
  }
}