import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSubordinadoComponent } from './table-subordinado.component';

describe('TableSubordinadoComponent', () => {
  let component: TableSubordinadoComponent;
  let fixture: ComponentFixture<TableSubordinadoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TableSubordinadoComponent]
    });
    fixture = TestBed.createComponent(TableSubordinadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
