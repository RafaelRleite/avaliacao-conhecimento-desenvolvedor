import { Component} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subordinado } from 'src/app/model/subordinado';
import { SubordinadoService } from 'src/app/services/subordinado.service';
import { ModalExcluirComponent } from '../modal-excluir/modal-excluir.component';
import { ModalEditarComponent } from '../modal-editar/modal-editar.component';
import { ModalCadastrarComponent } from '../modal-cadastrar/modal-cadastrar.component';
import { Page } from 'src/app/model/page';
import { PaginatorState } from 'primeng/paginator';
import { Presidente } from 'src/app/model/presidente';
import { Gerente } from 'src/app/model/gerente';

@Component({
  selector: 'app-table-subordinado',
  templateUrl: './table-subordinado.component.html',
  styleUrls: ['./table-subordinado.component.css']
})
export class TableSubordinadoComponent {
  subordinados:Array<Subordinado> = [];
  presidente!: Presidente;
  gerente!: Gerente;
  page!: Page;
  paginaAtual: number = 0;
  itensPorPagina: number = 10;
  totalLista!:number;

  constructor(private service:SubordinadoService, private modalService: NgbModal){}

  ngOnInit(): void {
    this.buscarSubordinados(this.paginaAtual, this.itensPorPagina);
  }

  buscarSubordinados(page: any, size: any) {
    this.service.getAllList(page, size).subscribe(data => {
      this.page = data;
      this.subordinados = this.page.content;
      this.totalLista = this.page.totalElements;
    });
  }

  onPageChange(event: PaginatorState) {
    this.buscarSubordinados(event.page, event.rows);
  }
  
  salvarSubordinado(){
    const modalRef = this.modalService.open(ModalCadastrarComponent);
  }

  deleteSubordinado(id: number) {
    const modalRef = this.modalService.open(ModalExcluirComponent);
    modalRef.componentInstance.id = id;
  }

  updateSubordinado(id: number) {
    const modalRef = this.modalService.open(ModalEditarComponent);
    modalRef.componentInstance.id = id;
  }

  toggleExpandido(item: Subordinado): void {
    this.subordinados.forEach((subordinado)=>{
      if(subordinado.expandido && subordinado != item){
        subordinado.expandido = false;
      }
    })
    item.expandido = !item.expandido;
    this.buscarPresidente(item.id);
    this.buscarGerente(item.id);
  }

  buscarPresidente(id:number){
    this.service.getPresidenteSubordinadoById(id).subscribe(data => {
     this.presidente = data;
    });
   }
 
   buscarGerente(id:number){
     this.service.getGerenteSubordinadoById(id).subscribe(data =>{
       this.gerente = data;
     })
   }
}
