import { formatDate } from '@angular/common';
import { Component, Injector, OnInit, } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Colaborador } from 'src/app/model/colaborador';
import { Gerente } from 'src/app/model/gerente';
import { Presidente } from 'src/app/model/presidente';
import { Subordinado } from 'src/app/model/subordinado';
import { GerenteService } from 'src/app/services/gerente.service';
import { PresidenteService } from 'src/app/services/presidente.service';
import { SubordinadoService } from 'src/app/services/subordinado.service';
import { RemuneracaoPipe } from '../pipe/remuneracao.pipe';


@Component({
  selector: 'app-modal-cadastrar',
  templateUrl: './modal-cadastrar.component.html',
  styleUrls: ['./modal-cadastrar.component.css'],
  providers: [RemuneracaoPipe]
})
export class ModalCadastrarComponent implements OnInit {

  id!: number;
  contador = 0;
  colaboradorSalvar: Colaborador | Gerente | Subordinado | Presidente = new Gerente;

  constructor(private serviceGerente: GerenteService, private serviceSubordinado: SubordinadoService,
    private servicePresidente: PresidenteService, public modal: NgbActiveModal, private injector: Injector) { }

  ngOnInit(): void {
    this.colaboradorSalvar.novaFuncao = "";
    this.colaboradorSalvar.dataAdmissao = this.formatarData();
    this.colaboradorSalvar.remuneracao = '';
  }

  salvar() {

    if (this.colaboradorSalvar.funcao.match("Gerente")) {
      this.serviceGerente.createGerente(this.colaboradorSalvar).subscribe(data => {
        this.colaboradorSalvar = data;
      });
    }
    else if (this.colaboradorSalvar.funcao.match("Subordinado")) {
      this.serviceSubordinado.createSubordinado(this.colaboradorSalvar).subscribe(data => {
        this.colaboradorSalvar = data;
      });
    }
    else if (this.colaboradorSalvar.funcao.match("Presidente")) {
      this.servicePresidente.createPresidente(this.colaboradorSalvar).subscribe(data => {
        this.colaboradorSalvar = data;
      });
    }
    this.recarregaList();
  }

  recarregaList() {
    this.modal.close();
    window.location.reload();
  }

  formatarData(): string {
    const data = new Date();
    const dataFormatada = formatDate(data, 'yyyy-MM-dd', 'en-US');
    return dataFormatada;
  }

  validarNumero(event: KeyboardEvent): boolean {
    const codigoTecla = event.key;
    if (/[0-9]/.test(codigoTecla) || codigoTecla === 'Tab') {
      this.contador = 0;
      return true;
    }
    else if (codigoTecla === 'Backspace' || codigoTecla === 'Tab') {
      this.contador = 1;
      return true;
    }
    return false;
  }

  validarString(event: KeyboardEvent): boolean {
    const codigoTecla = event.key;
    if (/[a-zA-Z]/.test(codigoTecla) || codigoTecla === 'Backspace') {
      return true;
    }
    return false;
  }

  atualizarRemuneracao(event: Event) {
    const valor = (event.target as HTMLInputElement).value;
    const remuneracaoPipe = this.injector.get(RemuneracaoPipe);
    const remuneracaoFormatada = remuneracaoPipe.transform(valor);
    this.colaboradorSalvar.remuneracao = remuneracaoFormatada;
  }
}