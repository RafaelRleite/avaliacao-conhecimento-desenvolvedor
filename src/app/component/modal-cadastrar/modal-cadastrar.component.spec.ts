import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCadastrarComponent } from './modal-cadastrar.component';

describe('ModalCadastrarComponent', () => {
  let component: ModalCadastrarComponent;
  let fixture: ComponentFixture<ModalCadastrarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModalCadastrarComponent]
    });
    fixture = TestBed.createComponent(ModalCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
