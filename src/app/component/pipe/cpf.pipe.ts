import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'CPF'
})
export class CPFPipe implements PipeTransform {

  transform(value: string): any {
    if (!value || value.length === 0) {
      return '';
    }

    if (value.length > 0 && value.length < 8) {
      return value.replace(/(\d{3})(\d{1})/g, '\$1.\$2');
    } else if (value.length >= 8) {
      return value.replace(/(\d{3})(\d{3})(\d{1})/g, '\$1.\$2-\$3');
    }
  }
} 