import { Pipe, PipeTransform } from '@angular/core';
import { ModalCadastrarComponent } from '../modal-cadastrar/modal-cadastrar.component';

@Pipe({
  name: 'remuneracao'
})
export class RemuneracaoPipe implements PipeTransform {

  constructor(private modal: ModalCadastrarComponent) { }

  transform(value: any): string {

    if (!value || value.length === 0 || this.modal.contador === 1) {
      return '';
    }

    let result = '';
    let tamanho = 0;
    const casasDecimais = 2;

    if (value.length <= 3) {
      result = value.replace('.', '').replace(',', '');
      tamanho = result.length;
    }
    else if (value.length > 3) {
      tamanho = value.replace('.', '').replace(',', '').length;
      let auxInteiro = value.split(',')[0].replace('.', '')
      let decimal = value.split(',')[1];
      result = auxInteiro + decimal[decimal.length - 1];
    }

    if (tamanho <= 3) {
      result = result.replace(/(\d)(\d)/g, '\$1,\$2');
    } else if (tamanho > 3 && tamanho <= 6) {
      result = result.replace(/(\d{1})(\d{3})/g, '\$1.\$2');
    } else if (tamanho === 7) {
      result = result.replace(/(\d{2})(\d{3})/g, '\$1.\$2');
    } else if (tamanho === 8) {
      result = result.replace(/(\d{3})(\d{3})/g, '\$1.\$2');
    }

    const partes = result.split(',');
    if (partes.length === 1) {
      result += ',00';
    } else if (partes[1].length < casasDecimais) {
      result += '0'.repeat(casasDecimais - partes[1].length);
    }

    console.log('Value: ' + value + '----> Result: ' + result)
    return result;
  }
}