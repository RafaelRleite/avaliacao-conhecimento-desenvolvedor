import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  constructor(private router: Router) { }

  navegarHome() {
    const url = this.router.url;

    if (url === '/colaboradores') {
      this.router.navigate(['']);
    } else if (url === '/') {
      this.router.navigate(['colaboradores']);
    } else { this.router.navigate(['']); }
  }
}