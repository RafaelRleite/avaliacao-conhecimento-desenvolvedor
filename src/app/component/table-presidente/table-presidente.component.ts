import { Component, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Presidente } from 'src/app/model/presidente';
import { PresidenteService } from 'src/app/services/presidente.service';
import { ModalExcluirComponent } from '../modal-excluir/modal-excluir.component';
import { ModalEditarComponent } from '../modal-editar/modal-editar.component';
import { ModalCadastrarComponent } from '../modal-cadastrar/modal-cadastrar.component';
import { Page } from 'src/app/model/page';
import { PaginatorState } from 'primeng/paginator';
import { Subordinado } from 'src/app/model/subordinado';
import { ModalRemoverComponent } from '../modal-remover/modal-remover.component';
import { Gerente } from 'src/app/model/gerente';
import { ModalAdicionarComponent } from '../modal-adicionar/modal-adicionar.component';

@Component({
  selector: 'app-table-presidente',
  templateUrl: './table-presidente.component.html',
  styleUrls: ['./table-presidente.component.css']
})
export class TablePresidenteComponent {
  presidentes: Array<Presidente> = [];
  subordinados:Array<Subordinado> = [];
  gerentes:Array<Gerente> = [];
  page!: Page;
  paginaAtual: number = 0;
  itensPorPagina: number = 10;
  totalLista!:number;

  constructor(private service: PresidenteService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.buscarPresidentes(this.paginaAtual, this.itensPorPagina);
  }

  buscarPresidentes(page: any, size: any) {
    this.service.getAllList(page, size).subscribe(data => {
      this.page = data;
      this.presidentes = this.page.content;
      this.totalLista = this.page.totalElements;
    });
  }

  onPageChange(event: PaginatorState) {
    this.buscarPresidentes(event.page, event.rows);
  }

  salvarPresidente(){
    const modalRef = this.modalService.open(ModalCadastrarComponent);
  }

  deletePresidente(id: number) {
    const modalRef = this.modalService.open(ModalExcluirComponent);
    modalRef.componentInstance.id = id;
  }

  updatePresidente(id: number) {
    const modalRef = this.modalService.open(ModalEditarComponent);
    modalRef.componentInstance.id = id;
  }

  adicionar(idAdicionador:number , adicionado:string){
    const modalRef = this.modalService.open(ModalAdicionarComponent);
    modalRef.componentInstance.idAdicionador = idAdicionador;
    modalRef.componentInstance.adicionado = adicionado;
  }

  toggleExpandido(item: Presidente): void {
    this.presidentes.forEach((presidente)=>{
      if(presidente.expandido && presidente != item){
        presidente.expandido = false;
      }
    })
    item.expandido = !item.expandido;
    this.buscarSubordinados(item.id);
    this.buscarGerentes(item.id);
  }

  buscarSubordinados(id:number){
   this.service.getSubordinadosPresidenteById(id).subscribe(data => {
    this.subordinados = data;
   });
  }

  buscarGerentes(id:number){
    this.service.getGerentesPresidenteById(id).subscribe(data =>{
      this.gerentes = data;
    })
  }

  remover(idRemovedor: number, idRemover: number){
    const modalRef = this.modalService.open(ModalRemoverComponent);
    modalRef.componentInstance.idRemovedor = idRemovedor;
    modalRef.componentInstance.idRemover = idRemover;
  }
}
