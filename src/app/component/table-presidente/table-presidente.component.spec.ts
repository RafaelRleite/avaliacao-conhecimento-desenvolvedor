import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablePresidenteComponent } from './table-presidente.component';

describe('TablePresidenteComponent', () => {
  let component: TablePresidenteComponent;
  let fixture: ComponentFixture<TablePresidenteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TablePresidenteComponent]
    });
    fixture = TestBed.createComponent(TablePresidenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
