import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/app/model/colaborador';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalExcluirComponent } from '../modal-excluir/modal-excluir.component';
import { ModalEditarComponent } from '../modal-editar/modal-editar.component';
import { ModalCadastrarComponent } from '../modal-cadastrar/modal-cadastrar.component';
import { Page } from 'src/app/model/page';
import { PaginatorState } from 'primeng/paginator';
import { timestamp } from 'rxjs';

@Component({
  selector: 'app-table-colaborador',
  templateUrl: './table-colaborador.component.html',
  styleUrls: ['./table-colaborador.component.css']
})
export class TableColaboradorComponent implements OnInit {
  colaboradores: Array<Colaborador> = [];
  filtroAno!: number;
  page!: Page;
  paginaAtual: number = 0;
  itensPorPagina: number = 10;
  totalLista!: number;

  constructor(private service: ColaboradorService, public modalService: NgbModal) { }

  ngOnInit(): void {
    this.buscarColaboradores(this.paginaAtual, this.itensPorPagina);
  }

  buscarColaboradores(page: any, size: any) {
    this.service.getAllList(page, size).subscribe(data => {
      this.page = data;
      this.colaboradores = this.page.content;
      this.totalLista = this.page.totalElements;
    });
  }

  onPageChange(event: PaginatorState) {
    this.buscarColaboradores(event.page, event.rows);
  }

  salvarColaborador() {
    const modalRef = this.modalService.open(ModalCadastrarComponent);
  }

  deleteColaborador(id: number) {
    const modalRef = this.modalService.open(ModalExcluirComponent);
    modalRef.componentInstance.id = id;
  }

  updateColaborador(id: number) {
    const modalRef = this.modalService.open(ModalEditarComponent);
    modalRef.componentInstance.id = id;
  }

  filtrarPorAno(filtroAno: number, page: any, size: any) {
    this.service.getFilterYear(this.filtroAno, page, size).subscribe(data => {
      this.page = data;
      this.colaboradores = this.page.content;
      this.totalLista = this.page.totalElements;
    });
  }

  onSubmit() {
    this.filtrarPorAno(this.filtroAno, this.paginaAtual, this.itensPorPagina);
  }
}