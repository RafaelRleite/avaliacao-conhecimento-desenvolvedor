import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableGerenteComponent } from './table-gerente.component';

describe('TableGerenteComponent', () => {
  let component: TableGerenteComponent;
  let fixture: ComponentFixture<TableGerenteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TableGerenteComponent]
    });
    fixture = TestBed.createComponent(TableGerenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
