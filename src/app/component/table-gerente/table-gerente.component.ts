import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Gerente } from 'src/app/model/gerente';
import { GerenteService } from 'src/app/services/gerente.service';
import { ModalExcluirComponent } from '../modal-excluir/modal-excluir.component';
import { ModalEditarComponent } from '../modal-editar/modal-editar.component';
import { ModalCadastrarComponent } from '../modal-cadastrar/modal-cadastrar.component';
import { Page } from 'src/app/model/page';
import { PaginatorState } from 'primeng/paginator';
import { Subordinado } from 'src/app/model/subordinado';
import { ModalRemoverComponent } from '../modal-remover/modal-remover.component';
import { ModalAdicionarComponent } from '../modal-adicionar/modal-adicionar.component';

@Component({
  selector: 'app-table-gerente',
  templateUrl: './table-gerente.component.html',
  styleUrls: ['./table-gerente.component.css']
})
export class TableGerenteComponent implements OnInit {
  gerentes: Array<Gerente> = [];
  subordinados: Array<Subordinado> = [];
  page!: Page;
  paginaAtual: number = 0;
  itensPorPagina: number = 10;
  totalLista!: number;

  constructor(private service: GerenteService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.buscarGerentes(this.paginaAtual, this.itensPorPagina);
  }

  buscarGerentes(page: any, size: any) {
    this.service.getAllList(page, size).subscribe(data => {
      this.page = data;
      this.gerentes = this.page.content;
      this.totalLista = this.page.totalElements;
    });
  }

  salvarGerente() {
    const modalRef = this.modalService.open(ModalCadastrarComponent);
  }

  deleteGerente(id: number) {
    const modalRef = this.modalService.open(ModalExcluirComponent);
    modalRef.componentInstance.id = id;
  }

  updateGerente(id: number) {
    const modalRef = this.modalService.open(ModalEditarComponent);
    modalRef.componentInstance.id = id;
  }

  adicionarSubordinado(idAdicionador:number, adicionado:string){
    const modalRef = this.modalService.open(ModalAdicionarComponent);
    modalRef.componentInstance.idAdicionador = idAdicionador;
    modalRef.componentInstance.adicionado = adicionado;
  }

  removerSubordinado(idRemovedor: number, idRemover: number) {
    const modalRef = this.modalService.open(ModalRemoverComponent);
    modalRef.componentInstance.idRemovedor = idRemovedor;
    modalRef.componentInstance.idRemover = idRemover;
  }

  buscarSubordinados(id: number) {
    this.service.getSubordinadosGerenteById(id).subscribe(data => {
      this.subordinados = data;
    });
  }

  toggleExpandido(item: Gerente): void {
    this.gerentes.forEach((gerente) => {
      if (gerente.expandido && gerente != item) {
        gerente.expandido = false;
      }
    })
    item.expandido = !item.expandido;
    this.buscarSubordinados(item.id);
  }

  onPageChange(event: PaginatorState) {
    this.buscarGerentes(event.page, event.rows);
  }
}