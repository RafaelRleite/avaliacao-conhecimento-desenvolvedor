import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Gerente } from 'src/app/model/gerente';
import { Presidente } from 'src/app/model/presidente';
import { Subordinado } from 'src/app/model/subordinado';
import { ColaboradorService } from 'src/app/services/colaborador.service';
import { GerenteService } from 'src/app/services/gerente.service';
import { PresidenteService } from 'src/app/services/presidente.service';
import { SubordinadoService } from 'src/app/services/subordinado.service';

@Component({
  selector: 'app-modal-adicionar',
  templateUrl: './modal-adicionar.component.html',
  styleUrls: ['./modal-adicionar.component.css']
})
export class ModalAdicionarComponent implements OnInit {
  idAdicionador!: number;
  adicionado!: string;
  colaboradorAdicionador: Gerente | Presidente = new Gerente;
  subordinados!: Subordinado[];
  gerentes!: Gerente[];
  colaboradorSelecionado!: Subordinado[];

  constructor(private service: ColaboradorService, private serviceGerente: GerenteService, private servicePresidente: PresidenteService
    , private serviceSubordinado: SubordinadoService, public modal: NgbActiveModal) { }

  ngOnInit(): void {
    this.getById();
    if(this.adicionado === "Subordinado"){
      this.buscarSubordinados();
    }else if(this.adicionado === "Gerente"){
      this.buscarGerentes();
    }
  }

  adicionar() {
    if (this.colaboradorAdicionador.funcao === "Gerente") {
      this.serviceGerente.adicionarSubordinados(this.idAdicionador, this.colaboradorSelecionado).subscribe(data => {
        this.subordinados = data;
      });
    } else if (this.colaboradorAdicionador.funcao === "Presidente") {
      if (this.adicionado === "Subordinado") {
        this.servicePresidente.adicionarSubordinados(this.idAdicionador, this.colaboradorSelecionado).subscribe(data => {
          this.subordinados = data;
        });
      } else if (this.adicionado === "Gerente") {
        this.servicePresidente.adicionarGerente(this.idAdicionador, this.colaboradorSelecionado).subscribe(data => {
          this.gerentes = data;
        });
      }
    }
    this.recarregaList();
  }

  buscarSubordinados() {
    this.serviceSubordinado.geSubordinados().subscribe(data => {
      this.subordinados = data;
    })
  }

  buscarGerentes() {
    this.serviceGerente.geGerentes().subscribe(data => {
      this.gerentes = data;
    })
  }

  getById() {
    this.service.getById(this.idAdicionador).subscribe(data => {
      this.colaboradorAdicionador = data;
    });
  }

  recarregaList() {
    this.modal.close();
    window.location.reload();
  }
}
