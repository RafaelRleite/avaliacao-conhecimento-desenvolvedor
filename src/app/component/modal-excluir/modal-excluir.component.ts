import { ColaboradorService } from '../../services/colaborador.service';
import { Colaborador } from '../../model/colaborador';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Gerente } from 'src/app/model/gerente';
import { Subordinado } from 'src/app/model/subordinado';
import { Presidente } from 'src/app/model/presidente';

@Component({
  selector: 'app-excluir-modal',
  templateUrl: './modal-excluir.component.html',
  styleUrls: ['./modal-excluir.component.css']
})

export class ModalExcluirComponent implements OnInit {

  id!: number;
  colaboradorExcluir: Colaborador | Gerente | Subordinado | Presidente = new Gerente;

  constructor(private service: ColaboradorService, public modal: NgbActiveModal) { }

  ngOnInit(): void {
    this.getById();
  }

  excluir() {
    this.service.delete(this.id).subscribe(data => {
      this.recarrecaList();
    });
  }

  getById() {
    this.service.getById(this.id).subscribe(data => {
      this.colaboradorExcluir = data;
    })
  }

  recarrecaList() {
    this.modal.close();
    window.location.reload();
  }

}