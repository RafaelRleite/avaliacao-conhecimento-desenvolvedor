import {DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { LoginComponent } from './component/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './component/home/home.component';
import { TableColaboradorComponent } from './component/table-colaborador/table-colaborador.component';
import { TableGerenteComponent } from './component/table-gerente/table-gerente.component';
import { ModalExcluirComponent } from './component/modal-excluir/modal-excluir.component';
import { ModalEditarComponent } from './component/modal-editar/modal-editar.component';
import { ModalCadastrarComponent } from './component/modal-cadastrar/modal-cadastrar.component';
import { TablePresidenteComponent } from './component/table-presidente/table-presidente.component';
import { TableSubordinadoComponent } from './component/table-subordinado/table-subordinado.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from './component/footer/footer.component';
import { CPFPipe } from './component/pipe/cpf.pipe';
import localePt from '@angular/common/locales/pt';
import {registerLocaleData} from '@angular/common';
import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { ModalRemoverComponent } from './component/modal-remover/modal-remover.component';
import { ModalAdicionarComponent } from './component/modal-adicionar/modal-adicionar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CheckboxModule } from 'primeng/checkbox';

registerLocaleData(localePt)

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    HomeComponent,
    TableColaboradorComponent,
    TableGerenteComponent,
    TableSubordinadoComponent,
    TablePresidenteComponent,
    ModalExcluirComponent,
    ModalEditarComponent,
    ModalCadastrarComponent,
    FooterComponent,
    CPFPipe,
    ModalRemoverComponent,
    ModalAdicionarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    NgxMaskDirective, NgxMaskPipe,
    PaginatorModule,
    TableModule,
    BrowserAnimationsModule,
    CheckboxModule,
  ],
  providers: [
    provideNgxMask(),
    { provide: LOCALE_ID, useValue: 'pt' },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'BRL' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}


