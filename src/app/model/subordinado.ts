import { Colaborador } from "./colaborador";
import { Gerente } from "./gerente";
import { Presidente } from "./presidente";

export class Subordinado extends Colaborador{
    override id!:number;
    gerente?:Gerente;
    presidente?: Presidente;
    expandido?: boolean;
}
