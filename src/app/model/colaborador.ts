export abstract class Colaborador {
    id!: number;
    cpf!: string;
    nome!: string;
    dataAdmissao!: Date | string;
    remuneracao!: string;
    funcao!: string;
    novaFuncao!: String;
}
