import { Colaborador } from "./colaborador";
import { Gerente } from "./gerente";
import { Subordinado } from "./subordinado";

export class Presidente extends Colaborador{
    override id!:number;
    gerentes?: Array<Gerente> =[];
    subordinados?: Array<Subordinado> = [];
    expandido?: boolean;
}
