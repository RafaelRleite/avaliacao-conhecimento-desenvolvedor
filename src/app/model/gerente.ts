import { Colaborador } from "./colaborador";
import { Presidente } from "./presidente";
import { Subordinado } from "./subordinado";

export class Gerente extends Colaborador{
    override id!:number;
    presidente?: Presidente;
    subordinados? : Array<Subordinado> = [];
    expandido?: boolean;
}
