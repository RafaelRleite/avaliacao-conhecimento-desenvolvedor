export interface Page {
  content: Array<Content>;
  pageable: Pageable;
  last: boolean;
  totalPages: number;
  totalElements: number;
  size: number;
  number: number;
  sort: Sort;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
};

export interface Content {
  id: number;
  nome: string;
  funcao: string;
  cpf: string;
  dataAdmissao: Date;
  remuneracao: string;
  novaFuncao: "";
};

export interface Pageable {
  sort: Sort;
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  unpaged: boolean;
};

export interface Sort {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
};

export interface Sort2 {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
};