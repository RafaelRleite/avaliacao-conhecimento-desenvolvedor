import { TableColaboradorComponent } from './component/table-colaborador/table-colaborador.component';
import { NgModule,} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { HomeComponent } from './component/home/home.component';
import { TableGerenteComponent } from './component/table-gerente/table-gerente.component';
import { TablePresidenteComponent } from './component/table-presidente/table-presidente.component';
import { TableSubordinadoComponent } from './component/table-subordinado/table-subordinado.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'gerentes', component: TableGerenteComponent },
  { path: 'colaboradores', component: TableColaboradorComponent },
  { path: 'subordinados', component: TableSubordinadoComponent },
  { path: 'presidentes', component: TablePresidenteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
