package br.com.memory.gerenciamentoFucionarios.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "subordinado")
@PrimaryKeyJoinColumn(name = "colaborador", foreignKey = @ForeignKey(name = "FK_colaborador_id"))
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Subordinado extends Colaborador {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "subordinado_seg_generetor", sequenceName = "subordinado_id_seg", allocationSize = 1)
	@GeneratedValue(generator = "subordinado_seg_generetor", strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "gerente", foreignKey = @ForeignKey(name = "FK_gerente_id"))
	@JsonBackReference
	private Gerente gerente;

	@ManyToOne
	@JoinColumn(name = "presidente", foreignKey = @ForeignKey(name = "FK_presidente_id"))
	@JsonIgnore
	private Presidente presidente;

}
