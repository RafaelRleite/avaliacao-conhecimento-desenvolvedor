package br.com.memory.gerenciamentoFucionarios.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.memory.gerenciamentoFucionarios.model.Gerente;

@Repository
public interface GerenteRepository extends JpaRepository<Gerente, Long> {

}
