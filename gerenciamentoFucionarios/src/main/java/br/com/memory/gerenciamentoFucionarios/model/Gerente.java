package br.com.memory.gerenciamentoFucionarios.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "gerente")
@PrimaryKeyJoinColumn(name = "colaborador", foreignKey = @ForeignKey(name = "FK_colaborador_id"))
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Gerente extends Colaborador {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "gerente_seg_generetor", sequenceName = "gerente_id_seg", allocationSize = 1)
	@GeneratedValue(generator = "gerente_seg_generetor", strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "presidente", foreignKey = @ForeignKey(name = "FK_presidente_id"))
	@JsonIgnore
	private Presidente presidente;

	@OneToMany(mappedBy = "gerente", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Subordinado> subordinados;

}
