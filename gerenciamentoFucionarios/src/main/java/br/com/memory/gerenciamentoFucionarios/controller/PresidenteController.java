package br.com.memory.gerenciamentoFucionarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.gerenciamentoFucionarios.dto.GerenteDTO;
import br.com.memory.gerenciamentoFucionarios.dto.PresidenteDTO;
import br.com.memory.gerenciamentoFucionarios.dto.SubordinadoDTO;
import br.com.memory.gerenciamentoFucionarios.model.Gerente;
import br.com.memory.gerenciamentoFucionarios.model.Presidente;
import br.com.memory.gerenciamentoFucionarios.model.Subordinado;
import br.com.memory.gerenciamentoFucionarios.service.PresidenteService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/")
public class PresidenteController {

	@Autowired
	private PresidenteService presidenteService;

	@PostMapping("/salvar-presidente")
	public ResponseEntity<Presidente> salvarPresidente(@RequestBody @Valid Presidente presidente) {
		return ResponseEntity.ok(presidenteService.salvar(presidente));
	}

	@PutMapping("/alterar-presidente/{id}")
	public ResponseEntity<Presidente> alterarPresidentePorId(@PathVariable("id") Long id, @RequestBody @Valid Presidente presidente) {
		return ResponseEntity.ok(presidenteService.alterarPresidentePorId(id, presidente));
	}

	@DeleteMapping("/delete-presidente/{id}")
	public ResponseEntity<Void> deletePresidentePorId(@PathVariable("id") Long id) {
		presidenteService.deletePresidentePorId(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/{presidenteId}/presidente/remover-gerentes")
	public ResponseEntity<Presidente> removerGerentesPresidentePorId(@PathVariable("presidenteId") Long presidenteId) {
		return ResponseEntity.ok(presidenteService.removerGerentesPresidentePorId(presidenteId));
	}

	@GetMapping("/{presidenteId}/presidente/remover-gerente/{gerenteId}")
	public ResponseEntity<Presidente> removerGerentePorIdPresidentePorId(@PathVariable("presidenteId") Long presidenteId, @PathVariable("gerenteId") Long gerenteId) {
		return ResponseEntity.ok(presidenteService.removerGerentePorIdPresidentePorId(presidenteId, gerenteId));
	}
	
	@PutMapping("/{presidenteId}/presidente/adicionar-gerentes")
	public ResponseEntity<List<GerenteDTO>> adicionarGerentesPresidentePorId (@PathVariable("presidenteId") Long id, @RequestBody @Valid List<Gerente> gerentes){
		return ResponseEntity.ok(presidenteService.adicionarGerentesPresidenteId(id, gerentes));
	}

	@GetMapping("/{presidenteId}/presidente/remover-subordinados")
	public ResponseEntity<Presidente> removerSubordinadosPresidentePorId(@PathVariable("presidenteId") Long presidenteId) {
		return ResponseEntity.ok(presidenteService.removerSubordinadosPresidentePorId(presidenteId));
	}

	@GetMapping("/{presidenteId}/presidente/remover-subordinado/{subordinadoId}")
	public ResponseEntity<Presidente> removerSubordinadoPorIdPresidentePorId(@PathVariable("presidenteId") Long presidenteId, @PathVariable("subordinadoId") Long subordinadoId) {
		return ResponseEntity.ok(presidenteService.removerSubordinadoPorIdPresidentePorId(presidenteId, subordinadoId));
	}
	
	@PutMapping("/{presidentId}/presidente/adicionar-subordinados")
	public ResponseEntity<List<SubordinadoDTO>> adicionarSubordinadosPresidentePorId (@PathVariable("presidentId") Long id, @RequestBody @Valid List<Subordinado> subordinados){
		return ResponseEntity.ok(presidenteService.adicionarSubordinadoPresidenteId(id, subordinados));
	}

	@GetMapping("/presidentes")
	@ResponseStatus(HttpStatus.OK)
	public Page<PresidenteDTO> buscarPresidentesPaginado(@PageableDefault(page = 0, size = 10, sort = { "id" }) Pageable paginacao) {
		return presidenteService.buscarPresidentesPaginado(paginacao);
	}
	
	@GetMapping("/presidentesAll")
	public ResponseEntity<List<PresidenteDTO>> buscarPresidentes(){
		return ResponseEntity.ok(presidenteService.buscarPresidentes());
	}

	@GetMapping("/presidente/{id}")
	public ResponseEntity<Presidente> buscarPresidentePorId(@PathVariable Long id) {
		return ResponseEntity.ok(presidenteService.buscarPresidentePorId(id));
	}

	@GetMapping("/{presidenteId}/presidente/gerentes")
	public ResponseEntity<List<Gerente>> buscarGerentesPresidentePorId(@PathVariable("presidenteId") Long id) {
		return ResponseEntity.ok(presidenteService.buscarGerentesPresidentePorId(id));
	}

	@GetMapping("/{presidenteId}/presidente/gerente/{gerenteId}")
	public ResponseEntity<Gerente> buscarGerentePorIdPresidentePorId(@PathVariable Long presidenteId, @PathVariable Long gerenteId) {
		return ResponseEntity.ok(presidenteService.buscarGerentePorIdPresidentePorId(presidenteId, gerenteId));
	}

	@GetMapping("/{presidenteId}/subordinados")
	public ResponseEntity<List<Subordinado>> buscarSubordinadosPresidentePorId(@PathVariable Long presidenteId) {
		return ResponseEntity.ok(presidenteService.buscarSubordinadosPresidentePorId(presidenteId));
	}

	@GetMapping("/{presidenteId}/presidente/subordinado/{subordinadoId}")
	public ResponseEntity<Subordinado> buscarSubordinadoPorIdPresidentePorId(@PathVariable Long presidenteId, @PathVariable Long subordinadoId) {
		return ResponseEntity.ok(presidenteService.buscarSubordinadoPorIdPresidentePorId(presidenteId, subordinadoId));
	}
}
