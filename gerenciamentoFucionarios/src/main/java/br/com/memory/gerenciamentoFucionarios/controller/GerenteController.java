package br.com.memory.gerenciamentoFucionarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.gerenciamentoFucionarios.dto.GerenteDTO;
import br.com.memory.gerenciamentoFucionarios.dto.SubordinadoDTO;
import br.com.memory.gerenciamentoFucionarios.model.Gerente;
import br.com.memory.gerenciamentoFucionarios.model.Presidente;
import br.com.memory.gerenciamentoFucionarios.model.Subordinado;
import br.com.memory.gerenciamentoFucionarios.service.GerenteService;
import jakarta.validation.Valid;

@RestController	
@RequestMapping("/api/v1")
public class GerenteController {

	@Autowired
	private GerenteService gerenteService;

	@PostMapping("/salvar-gerente")
	public ResponseEntity<Gerente> salvarGerente(@RequestBody @Valid Gerente gerente) {
		return ResponseEntity.ok(gerenteService.salvarGerente(gerente));
	}

	@PutMapping("/alterar-gerente/{id}")
	public ResponseEntity<Gerente> alterarGerentePorId(@PathVariable("id") Long id, @RequestBody @Valid Gerente gerente) {
		return ResponseEntity.ok(gerenteService.alterarGerentePorId(id, gerente));
	}
	
	@PutMapping("/{gerenteId}/gerente/adicionar-subordinados")
	public ResponseEntity<List<SubordinadoDTO>> adicionarSubordinadosGerentePorId (@PathVariable("gerenteId") Long id, @RequestBody @Valid List<Subordinado> subordinados){
		return ResponseEntity.ok(gerenteService.adicionarSubordinadosGerentePorId(id, subordinados));
	}

	@DeleteMapping("/delete-gerente/{id}")
	public ResponseEntity<Void> deleteGerentePorId(@PathVariable("id") Long id) {
		gerenteService.deleteGerentePorId(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/{gerenteId}/gerente/remover-subordinados")
	public ResponseEntity<Gerente> removerSubordinadosGerentePorId(@PathVariable("gerenteId") Long gerenteId) {
		return ResponseEntity.ok(gerenteService.removerSubordinadosGerentePorId(gerenteId));
	}

	@GetMapping("/{gerenteId}/gerente/remover-subordinado/{subordinadoId}")
	public ResponseEntity<Gerente> removerSubordinadoPorIdGerentePorId(@PathVariable("gerenteId") Long gerenteId, @PathVariable("subordinadoId") Long subordinadoId) {
		return ResponseEntity.ok(gerenteService.removerSubordinadoPorIdGerentePorId(gerenteId, subordinadoId));
	}

	@GetMapping("/gerentes")
	@ResponseStatus(HttpStatus.OK)
	public Page<GerenteDTO> buscarGerentesPaginado(@PageableDefault(page = 0, size = 10, sort = { "id" }) Pageable paginacao) {
		return gerenteService.buscarGerentesPaginado(paginacao);
	}
	
	@GetMapping("/gerentesAll")
	public ResponseEntity<List<GerenteDTO>> buscarGerentes(){
		return ResponseEntity.ok(gerenteService.buscarGerentes());
	}

	@GetMapping("/gerente/{id}")
	public ResponseEntity<Gerente> buscarGerentePorId(@PathVariable("id") Long id) {
		return ResponseEntity.ok(gerenteService.buscarGerentePorId(id));
	}

	@GetMapping("/{gerenteId}/gerente/presidente")
	public ResponseEntity<Presidente> buscarPresidenteGerentePorId(@PathVariable("gerenteId") Long gerenteId) {
		return ResponseEntity.ok(gerenteService.buscarPresidenteGerentePorId(gerenteId));
	}

	@GetMapping("/{gerenteId}/gerente/subordinados")
	public ResponseEntity<List<Subordinado>> buscarSubordinadosGerentePorId(@PathVariable("gerenteId") Long id) {
		return ResponseEntity.ok(gerenteService.buscarSubordinadosGerentePorId(id));
	}

	@GetMapping("/{gerenteId}/gerente/subordinado/{subordinadoId}")
	public ResponseEntity<Subordinado> buscarSubordinadoPorIdGerentePorId(@PathVariable("gerenteId") Long gerenteId, @PathVariable("subordinadoId") Long subordinadoId) {
		return ResponseEntity.ok(gerenteService.buscarSubordinadoPorIdGerentePorId(gerenteId, subordinadoId));
	}
}
