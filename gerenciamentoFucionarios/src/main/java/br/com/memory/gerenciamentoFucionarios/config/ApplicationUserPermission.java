package br.com.memory.gerenciamentoFucionarios.config;

public enum ApplicationUserPermission {
	USER_READ("user:read"),
	USER_WRITE("user:wride");
	
	private final String permission;
	
	private ApplicationUserPermission(String permission) {
		this.permission = permission;
	}
	
	public String getPermission() {
		return permission;
	}
}
