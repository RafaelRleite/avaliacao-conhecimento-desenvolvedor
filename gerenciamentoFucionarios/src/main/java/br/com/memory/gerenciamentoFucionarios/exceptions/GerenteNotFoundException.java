package br.com.memory.gerenciamentoFucionarios.exceptions;

public class GerenteNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public GerenteNotFoundException(Long id) {
		super("Gerente com o ID: " + id + " nâo encontrado.");
	}
	

}
