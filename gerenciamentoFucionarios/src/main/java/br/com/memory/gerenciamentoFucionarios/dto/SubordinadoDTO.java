package br.com.memory.gerenciamentoFucionarios.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.memory.gerenciamentoFucionarios.model.Subordinado;

public record SubordinadoDTO(Long id, String nome, String funcao, String cpf, LocalDate dataAdmissao, BigDecimal remuneracao, String novaFuncao) {
	public SubordinadoDTO(Subordinado subordinado) {
		this(subordinado.getId(), subordinado.getNome(), subordinado.getFuncao(), subordinado.getCpf(), subordinado.getDataAdmissao(), subordinado.getRemuneracao(),
				subordinado.getNovaFuncao());
	}

	public Long id() {
		return id;
	}

	public String nome() {
		return nome;
	}

	public String funcao() {
		return funcao;
	}

	public String cpf() {
		return cpf;
	}

	public LocalDate dataAdmissao() {
		return dataAdmissao;
	}

	public BigDecimal remuneracao() {
		return remuneracao;
	}

	public String novaFuncao() {
		return novaFuncao;
	}

}
