package br.com.memory.gerenciamentoFucionarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.memory.gerenciamentoFucionarios.dto.ColaboradorDTO;
import br.com.memory.gerenciamentoFucionarios.exceptions.ColaboradorNotFoundException;
import br.com.memory.gerenciamentoFucionarios.model.Colaborador;
import br.com.memory.gerenciamentoFucionarios.model.Gerente;
import br.com.memory.gerenciamentoFucionarios.model.Presidente;
import br.com.memory.gerenciamentoFucionarios.model.Subordinado;
import br.com.memory.gerenciamentoFucionarios.repository.ColaboradorRepositry;

@Service
public class ColaboradorService {

	@Autowired
	ColaboradorRepositry colaboradorRepositry;

	public Colaborador salvarColaborador(Colaborador colaborador) {
		return colaboradorRepositry.save(colaborador);
	}

	public void deleteColaboradorPorId(Long id) {
		buscarColaboradorPorId(id);
		colaboradorRepositry.deleteById(id);
	}

	public Colaborador atualizarDadosColaborador(Colaborador colaboradorBanco, Colaborador colaboradorAtualizar) {
		if (colaboradorAtualizar.getCpf() != null) {
			colaboradorBanco.setCpf(colaboradorAtualizar.getCpf());
		}
		if (colaboradorAtualizar.getDataAdmissao() != null) {
			colaboradorBanco.setDataAdmissao(colaboradorAtualizar.getDataAdmissao());
		}
		if (colaboradorAtualizar.getFuncao() != null) {
			colaboradorBanco.setFuncao(colaboradorAtualizar.getFuncao());
		}
		if (colaboradorAtualizar.getNome() != null) {
			colaboradorBanco.setNome(colaboradorAtualizar.getNome());
		}
		if (colaboradorAtualizar.getRemuneracao() != null) {
			colaboradorBanco.setRemuneracao(colaboradorAtualizar.getRemuneracao());
		}
		return colaboradorBanco;
	}

	public Page<ColaboradorDTO> filtrarPorAno(Integer ano, Pageable paginacao) {
		List<Colaborador> colaboradores = colaboradorRepositry.findAll();

		List<ColaboradorDTO> colaboradoresFiltrados = colaboradores.stream().filter(colaboradorLista -> colaboradorLista.getDataAdmissao().getYear() == ano)
				.map(ColaboradorDTO::new).toList();
		
		return new PageImpl<>(colaboradoresFiltrados, paginacao, colaboradoresFiltrados.size());
	}

	public Page<ColaboradorDTO> buscarColaboradores(Pageable paginancao) {
		Page<ColaboradorDTO> colaboradores = colaboradorRepositry.findAll(paginancao).map(ColaboradorDTO::new);
		return colaboradores;
	}
	
	public Colaborador buscarColaboradorPorId(Long id) {
		return colaboradorRepositry.findById(id).orElseThrow(() -> new ColaboradorNotFoundException(id));
	}

	public Colaborador transformarGerenteEmSubordinado(Gerente gerente) {
		Subordinado subordinado = new Subordinado();
		subordinado = (Subordinado) transferirDadosColaborador(gerente, subordinado);
		subordinado.setFuncao("Subordinado");
		colaboradorRepositry.delete(gerente);
		return salvarColaborador(subordinado);
	}

	public Colaborador transformarGerenteEmPresidente(Gerente gerente) {
		Presidente presidente = new Presidente();
		presidente = (Presidente) transferirDadosColaborador(gerente, presidente);
		presidente.setFuncao("Presidente");
		colaboradorRepositry.delete(gerente);
		return salvarColaborador(presidente);
	}

	public Colaborador transformarSubordinadoEmGerente(Subordinado subordinado) {
		Gerente gerente = new Gerente();
		gerente = (Gerente) transferirDadosColaborador(subordinado, gerente);
		gerente.setFuncao("Gerente");
		colaboradorRepositry.delete(subordinado);
		return salvarColaborador(gerente);
	}

	public Colaborador transformarSubordinadoEmPresidente(Subordinado subordinado) {
		Presidente presidente = new Presidente();
		presidente = (Presidente) transferirDadosColaborador(subordinado, presidente);
		presidente.setFuncao("Presidente");
		colaboradorRepositry.delete(subordinado);
		return salvarColaborador(presidente);
	}

	public Colaborador transformarPresidenteEmGerente(Presidente presidente) {
		Gerente gerente = new Gerente();
		gerente = (Gerente) transferirDadosColaborador(presidente, gerente);
		gerente.setFuncao("Gerente");
		colaboradorRepositry.delete(presidente);
		return salvarColaborador(gerente);
	}

	public Colaborador transformarPresidenteEmSubordinado(Presidente presidente) {
		Subordinado subordinado = new Subordinado();
		subordinado = (Subordinado) transferirDadosColaborador(presidente, subordinado);
		subordinado.setFuncao("Subordinado");
		colaboradorRepositry.delete(presidente);
		return salvarColaborador(subordinado);
	}

	private Colaborador transferirDadosColaborador(Colaborador colaboradorAntigo, Colaborador colaboradorNovo) {
		if (colaboradorAntigo.getCpf() != null) {
			colaboradorNovo.setCpf(colaboradorAntigo.getCpf());
		}
		if (colaboradorAntigo.getDataAdmissao() != null) {
			colaboradorNovo.setDataAdmissao(colaboradorAntigo.getDataAdmissao());
		}
		if (colaboradorAntigo.getNome() != null) {
			colaboradorNovo.setNome(colaboradorAntigo.getNome());
		}
		if (colaboradorAntigo.getRemuneracao() != null) {
			colaboradorNovo.setRemuneracao(colaboradorAntigo.getRemuneracao());
		}
		colaboradorNovo.setNovaFuncao("");

		return colaboradorNovo;
	}
}
