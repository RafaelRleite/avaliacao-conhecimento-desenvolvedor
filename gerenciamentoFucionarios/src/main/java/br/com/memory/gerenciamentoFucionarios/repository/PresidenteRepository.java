package br.com.memory.gerenciamentoFucionarios.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.memory.gerenciamentoFucionarios.model.Presidente;

@Repository
public interface PresidenteRepository extends JpaRepository<Presidente, Long> {

}
