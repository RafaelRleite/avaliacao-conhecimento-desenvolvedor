package br.com.memory.gerenciamentoFucionarios.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.memory.gerenciamentoFucionarios.dto.GerenteDTO;
import br.com.memory.gerenciamentoFucionarios.dto.PresidenteDTO;
import br.com.memory.gerenciamentoFucionarios.dto.SubordinadoDTO;
import br.com.memory.gerenciamentoFucionarios.exceptions.GerenteNotFoundException;
import br.com.memory.gerenciamentoFucionarios.exceptions.PresidenteNotFoundException;
import br.com.memory.gerenciamentoFucionarios.exceptions.SubordinadoNotFoundException;
import br.com.memory.gerenciamentoFucionarios.model.Gerente;
import br.com.memory.gerenciamentoFucionarios.model.Presidente;
import br.com.memory.gerenciamentoFucionarios.model.Subordinado;
import br.com.memory.gerenciamentoFucionarios.repository.PresidenteRepository;
import br.com.memory.gerenciamentoFucionarios.repository.SubordinadoRepository;

@Service
public class PresidenteService extends ColaboradorService {

	@Autowired
	private PresidenteRepository presidenteRepository;
	
	@Autowired
	private SubordinadoRepository subordinadoRepository;

	public Presidente salvar(Presidente presidente) {
		return presidenteRepository.save(presidente);
	}

	public void deletePresidentePorId(Long id) {
		buscarPresidentePorId(id);
		presidenteRepository.deleteById(id);
	}

	public Presidente alterarPresidentePorId(Long id, Presidente presidenteAtualizado) {
		Presidente presidenteBanco = buscarPresidentePorId(id);
		presidenteBanco = (Presidente) super.atualizarDadosColaborador(presidenteBanco, presidenteAtualizado);

		if (presidenteAtualizado.getNovaFuncao() != null && presidenteAtualizado.getNovaFuncao() != "") {
			if (presidenteAtualizado.getNovaFuncao().equals("Gerente")) {
				super.transformarPresidenteEmGerente(presidenteBanco);
				return null;

			}
			if (presidenteAtualizado.getNovaFuncao().equals("Subordinado")) {
				super.transformarPresidenteEmSubordinado(presidenteBanco);
				return null;
			}
		}

		if (presidenteAtualizado.getSubordinados() != null && !presidenteAtualizado.getSubordinados().isEmpty()) {
			Set<Long> subordinadosIdsBanco = presidenteBanco.getSubordinados().stream().map(Subordinado::getId).collect(Collectors.toSet());

			presidenteAtualizado.getSubordinados().stream().filter(subordinadoAtualizado -> !subordinadosIdsBanco.contains(subordinadoAtualizado.getId()))
					.forEach(presidenteBanco.getSubordinados()::add);
		}

		if (presidenteAtualizado.getGerentes() != null && !presidenteAtualizado.getGerentes().isEmpty()) {
			Set<Long> gerentesIdsBanco = presidenteBanco.getGerentes().stream().map(Gerente::getId).collect(Collectors.toSet());

			presidenteAtualizado.getGerentes().stream().filter(gerenteAtualizado -> !gerentesIdsBanco.contains(presidenteAtualizado.getId()))
					.forEach(presidenteBanco.getGerentes()::add);
		}

		return presidenteRepository.save(presidenteBanco);
	}

	public Presidente removerGerentesPresidentePorId(Long id) {
		Presidente presidente = buscarPresidentePorId(id);
		presidente.getGerentes().stream().forEach(gerente -> gerente.setPresidente(null));
		presidente.getGerentes().clear();
		return salvar(presidente);
	}

	public Presidente removerGerentePorIdPresidentePorId(Long presidenteId, Long gerenteId) {
		Presidente presidente = buscarPresidentePorId(presidenteId);
		List<Gerente> gerentes = presidente.getGerentes();

		Gerente gerenteRemover = gerentes.stream().filter(subordinadoLista -> subordinadoLista.getId().equals(gerenteId)).findFirst()
				.orElseThrow(() -> new GerenteNotFoundException(gerenteId));

		gerenteRemover.setPresidente(null);
		gerentes.remove(gerenteRemover);
		presidente.setGerentes(gerentes);
		return salvar(presidente);
	}
	
	public List<GerenteDTO> adicionarGerentesPresidenteId(Long id, List<Gerente> gerentes){
		Presidente presidente = buscarPresidentePorId(id);
		gerentes.stream().forEach(gerente -> gerente.setPresidente(presidente));
		
		presidente.getGerentes().addAll(gerentes);
		
		salvar(presidente);
		
		return presidente.getGerentes().stream().map(GerenteDTO::new).toList();
	}

	public Presidente removerSubordinadosPresidentePorId(Long id) {
		Presidente presidente = buscarPresidentePorId(id);
		presidente.getSubordinados().stream().forEach(subordinado -> subordinado.setPresidente(null));
		presidente.getSubordinados().clear();
		return salvar(presidente);
	}

	public Presidente removerSubordinadoPorIdPresidentePorId(Long presidenteId, Long subordinadoId) {
		Presidente presidente = buscarPresidentePorId(presidenteId);
		List<Subordinado> subordinados = presidente.getSubordinados();

		Subordinado subordinadoRemover = subordinados.stream().filter(subordinadoLista -> subordinadoLista.getId().equals(subordinadoId)).findFirst()
				.orElseThrow(() -> new SubordinadoNotFoundException(subordinadoId));

		subordinadoRemover.setPresidente(null);
		subordinados.remove(subordinadoRemover);
		presidente.setSubordinados(subordinados);
		return salvar(presidente);
	}
	
	public List<SubordinadoDTO> adicionarSubordinadoPresidenteId(Long id, List<Subordinado> subordinados){
		Presidente presidente = buscarPresidentePorId(id);
		List<Subordinado> subordinadosBanco = subordinadoRepository.findAll();
		Map<Long, Subordinado> subordinadosBancoMap = subordinadosBanco.stream()
	            .collect(Collectors.toMap(Subordinado::getId, Function.identity()));
		
		subordinados.forEach(subordinado -> {
	        if (subordinadosBancoMap.containsKey(subordinado.getId())) {
	            Subordinado subordinadoBanco = subordinadosBancoMap.get(subordinado.getId());
	            subordinado.setGerente(subordinadoBanco.getGerente());
	            subordinado.setPresidente(presidente);
	        }
	    });
		
		presidente.getSubordinados().addAll(subordinados);
		
		salvar(presidente);
		
		return presidente.getSubordinados().stream().map(SubordinadoDTO::new).toList();
	}

	
	public Page<PresidenteDTO> buscarPresidentesPaginado(Pageable paginacao) {
		Page<PresidenteDTO> presidentes = presidenteRepository.findAll(paginacao).map(PresidenteDTO::new);
		return presidentes;
	}
	
	public List<PresidenteDTO> buscarPresidentes() {
		List<PresidenteDTO> presidentes = presidenteRepository.findAll().stream().map(PresidenteDTO::new).toList();
		return presidentes;
	}

	public Presidente buscarPresidentePorId(Long id) {
		return presidenteRepository.findById(id).orElseThrow(() -> new PresidenteNotFoundException(id));
	}

	public List<Gerente> buscarGerentesPresidentePorId(Long id) {
		Presidente presidente = buscarPresidentePorId(id);
		return presidente.getGerentes();
	}

	public Gerente buscarGerentePorIdPresidentePorId(Long presidenteId, Long gerenteId) {
		Presidente presidente = buscarPresidentePorId(presidenteId);

		Gerente gerente = presidente.getGerentes().stream().filter(gerenteLista -> gerenteLista.getId().equals(gerenteId)).findFirst()
				.orElseThrow(() -> new GerenteNotFoundException(gerenteId));
		return gerente;
	}

	public List<Subordinado> buscarSubordinadosPresidentePorId(Long id) {
		Presidente presidente = buscarPresidentePorId(id);
		return presidente.getSubordinados();
	}

	public Subordinado buscarSubordinadoPorIdPresidentePorId(Long PresidenteId, Long subordinadoId) {
		Presidente presidente = buscarPresidentePorId(PresidenteId);

		Subordinado subordinado = presidente.getSubordinados().stream().filter(subordinadoLista -> subordinadoLista.getId().equals(subordinadoId)).findFirst()
				.orElseThrow(() -> new SubordinadoNotFoundException(subordinadoId));
		return subordinado;
	}

}
