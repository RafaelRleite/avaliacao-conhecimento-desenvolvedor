package br.com.memory.gerenciamentoFucionarios.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import br.com.memory.gerenciamentoFucionarios.model.Gerente;
import br.com.memory.gerenciamentoFucionarios.model.Subordinado;

public record GerenteDTO(Long id, String nome, String funcao, String cpf, LocalDate dataAdmissao, BigDecimal remuneracao, String novaFuncao, List<Subordinado> subordinado) {

	public GerenteDTO(Gerente gerente) {
		this(gerente.getId(), gerente.getNome(), gerente.getFuncao(), gerente.getCpf(), gerente.getDataAdmissao(), gerente.getRemuneracao(), gerente.getNovaFuncao(), gerente.getSubordinados());
	}

	public Long id() {
		return id;
	}

	public String nome() {
		return nome;
	}

	public String funcao() {
		return funcao;
	}

	public String cpf() {
		return cpf;
	}

	public LocalDate dataAdmissao() {
		return dataAdmissao;
	}

	public BigDecimal remuneracao() {
		return remuneracao;
	}

	public String novaFuncao() {
		return novaFuncao;
	}

	public List<Subordinado> subordinado() {
		return subordinado;
	}
	
}
