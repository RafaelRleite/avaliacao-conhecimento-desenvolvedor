package br.com.memory.gerenciamentoFucionarios.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.memory.gerenciamentoFucionarios.model.Colaborador;
@Repository
public interface ColaboradorRepositry extends JpaRepository<Colaborador, Long>{

}
