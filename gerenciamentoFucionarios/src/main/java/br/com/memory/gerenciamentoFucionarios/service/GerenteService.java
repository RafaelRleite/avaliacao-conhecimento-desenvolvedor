package br.com.memory.gerenciamentoFucionarios.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.memory.gerenciamentoFucionarios.dto.GerenteDTO;
import br.com.memory.gerenciamentoFucionarios.dto.SubordinadoDTO;
import br.com.memory.gerenciamentoFucionarios.exceptions.GerenteNotFoundException;
import br.com.memory.gerenciamentoFucionarios.exceptions.SubordinadoNotFoundException;
import br.com.memory.gerenciamentoFucionarios.model.Gerente;
import br.com.memory.gerenciamentoFucionarios.model.Presidente;
import br.com.memory.gerenciamentoFucionarios.model.Subordinado;
import br.com.memory.gerenciamentoFucionarios.repository.GerenteRepository;
import br.com.memory.gerenciamentoFucionarios.repository.SubordinadoRepository;

@Service
public class GerenteService extends ColaboradorService {

	@Autowired
	private GerenteRepository gerenteRepository;
	
	@Autowired
	private SubordinadoRepository subordinadoRepository;

	public Gerente salvarGerente(Gerente gerente) {
		return gerenteRepository.save(gerente);
	}

	public void deleteGerentePorId(Long id) {
		buscarGerentePorId(id);
		gerenteRepository.deleteById(id);
	}

	public Gerente alterarGerentePorId(Long id, Gerente gerenteAtualizar) {
		Gerente gerenteBanco = buscarGerentePorId(id);
		gerenteBanco = (Gerente) super.atualizarDadosColaborador(gerenteBanco, gerenteAtualizar);

		if (gerenteAtualizar.getNovaFuncao() != null && gerenteAtualizar.getNovaFuncao() != "") {
			if (gerenteAtualizar.getNovaFuncao().equals("Subordinado")) {
				super.transformarGerenteEmSubordinado(gerenteBanco);
				return null;
			}
			if (gerenteAtualizar.getNovaFuncao().equals("Presidente")) {
				super.transformarGerenteEmPresidente(gerenteBanco);
				return null;
			}
		}

		if (gerenteAtualizar.getSubordinados() != null && !gerenteAtualizar.getSubordinados().isEmpty()) {
			Set<Long> subordinadosIdsBanco = gerenteBanco.getSubordinados().stream().map(Subordinado::getId).collect(Collectors.toSet());

			gerenteAtualizar.getSubordinados().stream().filter(subordinadoAtualizado -> !subordinadosIdsBanco.contains(subordinadoAtualizado.getId()))
					.forEach(gerenteBanco.getSubordinados()::add);
		}
		if (gerenteAtualizar.getPresidente() != null) {
			gerenteBanco.setPresidente(gerenteAtualizar.getPresidente());
		}

		return salvarGerente(gerenteBanco);
	}

	public Gerente removerSubordinadosGerentePorId(Long id) {
		Gerente gerente = buscarGerentePorId(id);
		gerente.getSubordinados().stream().forEach(subordinado -> subordinado.setGerente(null));
		gerente.getSubordinados().clear();
		return salvarGerente(gerente);
	}

	public Gerente removerSubordinadoPorIdGerentePorId(Long gerenteId, Long subordinadoId) {
		Gerente gerente = buscarGerentePorId(gerenteId);
		List<Subordinado> subordinados = gerente.getSubordinados();

		Subordinado subordinadoRemover = subordinados.stream().filter(subordinadoLista -> subordinadoLista.getId().equals(subordinadoId)).findFirst()
				.orElseThrow(() -> new SubordinadoNotFoundException(subordinadoId));

		subordinadoRemover.setGerente(null);		
		subordinados.remove(subordinadoRemover);
		gerente.setSubordinados(subordinados);
		return salvarGerente(gerente);
	}
	
	public List<SubordinadoDTO> adicionarSubordinadosGerentePorId(Long id, List<Subordinado> subordinados) {
		Gerente gerente = buscarGerentePorId(id);
		List<Subordinado> subordinadosBanco = subordinadoRepository.findAll();
		
		Map<Long, Subordinado> subordinadoBancoMap = subordinadosBanco.stream()
				.collect(Collectors.toMap(Subordinado::getId, Function.identity()));
		
		subordinados.forEach(subordinado -> {
			if(subordinadoBancoMap.containsKey(subordinado.getId())) {
				Subordinado subordinadoBanco = subordinadoBancoMap.get(subordinado.getId());
				subordinado.setPresidente(subordinadoBanco.getPresidente());
				subordinado.setGerente(gerente);
			}
		});
		
		gerente.getSubordinados().addAll(subordinados);
		
		salvarGerente(gerente);
				
		return gerente.getSubordinados().stream().map(SubordinadoDTO::new).toList();
	}

	public Page<GerenteDTO> buscarGerentesPaginado(Pageable paginacao) {
		Page<GerenteDTO> gerentes = gerenteRepository.findAll(paginacao).map(GerenteDTO::new);
		return gerentes;
	}
	
	public List<GerenteDTO> buscarGerentes() {
		List<GerenteDTO> gerentes = gerenteRepository.findAll().stream().map(GerenteDTO::new).toList();
		return gerentes;
	}

	public Gerente buscarGerentePorId(Long id) {
		return gerenteRepository.findById(id).orElseThrow(() -> new GerenteNotFoundException(id));
	}

	public Presidente buscarPresidenteGerentePorId(Long id) {
		Gerente gerente = buscarGerentePorId(id);
		return gerente.getPresidente();
	}

	public List<Subordinado> buscarSubordinadosGerentePorId(Long id) {
		Gerente gerente = buscarGerentePorId(id);
		return gerente.getSubordinados();
	}

	public Subordinado buscarSubordinadoPorIdGerentePorId(Long gerenteId, Long subordinadoId) {
		Gerente gerente = buscarGerentePorId(gerenteId);

		Subordinado subordinado = gerente.getSubordinados().stream().filter(subordinadoLista -> subordinadoLista.getId().equals(subordinadoId)).findFirst()
				.orElseThrow(() -> new SubordinadoNotFoundException(subordinadoId));
		return subordinado;
	}

	

}
