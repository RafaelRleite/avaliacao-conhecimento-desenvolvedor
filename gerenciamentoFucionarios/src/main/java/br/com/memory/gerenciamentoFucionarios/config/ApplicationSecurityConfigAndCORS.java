package br.com.memory.gerenciamentoFucionarios.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import static br.com.memory.gerenciamentoFucionarios.config.ApplicationUserRole.*;
import static br.com.memory.gerenciamentoFucionarios.config.ApplicationUserPermission.*;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfigAndCORS  {

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.authorizeHttpRequests()
			.requestMatchers("/login", "/", "index", "/css/*", "/js/*").permitAll()
//			.requestMatchers("/api/**").hasRole(USER.name())
			.requestMatchers("/api/**").hasAnyRole(USER.name(), ADMIN.name(), ADMINJUNIOR.name())
			.requestMatchers(HttpMethod.DELETE, "/api/**").hasAuthority(USER_WRITE.getPermission())
			.requestMatchers(HttpMethod.POST, "/api/**").hasAuthority(USER_WRITE.getPermission())
			.requestMatchers(HttpMethod.PUT, "/api/**").hasAuthority(USER_WRITE.getPermission())
			.anyRequest()
			.authenticated()
			.and()
			.httpBasic();

		return http.build();
	}
	
	@Bean
	public UserDetailsManager users() {
		
		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		
		UserDetails user = User.builder()
				.username("rafael")
				.password(passwordEncoder.encode("123456789"))
//				.roles(USER.name())
				.authorities(USER.getGrantedAuthorities())
				.build();
		UserDetails admin = User.builder()
				.username("#admin")
				.password(passwordEncoder.encode("123456789"))
//				.roles(ADMIN.name())
				.authorities(ADMIN.getGrantedAuthorities())
				.build();
		UserDetails adminJunior = User.builder()
				.username("#adminJunior")
				.password(passwordEncoder.encode("123456789"))
//				.roles(ADMINJUNIOR.name())
				.authorities(ADMINJUNIOR.getGrantedAuthorities())
				.build();
		
		return new InMemoryUserDetailsManager(user, admin, adminJunior);
	}

	@Bean
	public CorsFilter corsFilter() {
		CorsConfiguration config = new CorsConfiguration();
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		config.addAllowedOrigin("*");

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);

		return new CorsFilter(source);
	}
}