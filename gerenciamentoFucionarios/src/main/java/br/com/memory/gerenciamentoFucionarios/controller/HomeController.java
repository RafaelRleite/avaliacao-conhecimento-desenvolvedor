package br.com.memory.gerenciamentoFucionarios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.gerenciamentoFucionarios.dto.ColaboradorDTO;
import br.com.memory.gerenciamentoFucionarios.model.Colaborador;
import br.com.memory.gerenciamentoFucionarios.service.ColaboradorService;

@RestController
@RequestMapping("/api/v1")
public class HomeController {

	@Autowired
	ColaboradorService colaboradorService;

	@GetMapping("/home")
	@ResponseStatus(HttpStatus.OK)
	public Page<ColaboradorDTO> home(@PageableDefault(page = 0, size = 10, sort = { "id" }) Pageable paginacao) {
		return colaboradorService.buscarColaboradores(paginacao);
	}

	@GetMapping("/filtrar/{ano}")
	public Page<ColaboradorDTO> filtrarPorAno(@PathVariable("ano") Integer ano, @PageableDefault(page = 0, size = 10, sort = { "id" }) Pageable paginacao) {
		return colaboradorService.filtrarPorAno(ano, paginacao);
	}

/*
 * Formas diferentes de responder um CRUD no Controller -> DELETE
 */
//	@DeleteMapping("/delete-colaborador/{id}")
//	public ResponseEntity<Void> deleteColaboradorPorId(@PathVariable("id") Long id) {
//		colaboradorService.deleteColaboradorPorId(id);
//		return ResponseEntity.noContent().build();
//	}

/*
 * Formas diferentes de responder um CRUD no Controller -> DELETE
 */
	@DeleteMapping("/delete-colaborador/{id}")
	public ResponseEntity<Object> deleteColaboradorPorId(@PathVariable("id") Long id) {
		colaboradorService.deleteColaboradorPorId(id);
		return new ResponseEntity<>("Colaborador deletado com sucesso", HttpStatus.OK);
	}

	@GetMapping("/colaborador/{id}")
	public ResponseEntity<Colaborador> buscarColaboradorPorId(@PathVariable("id") Long id) {
		return ResponseEntity.ok(colaboradorService.buscarColaboradorPorId(id));
	}
}
