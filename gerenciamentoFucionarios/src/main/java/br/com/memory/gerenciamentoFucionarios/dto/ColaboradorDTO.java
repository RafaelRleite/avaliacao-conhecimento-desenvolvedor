package br.com.memory.gerenciamentoFucionarios.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.memory.gerenciamentoFucionarios.model.Colaborador;

public record ColaboradorDTO(Long id, String nome, String funcao, String cpf, LocalDate dataAdmissao, BigDecimal remuneracao, String novaFuncao) {

	public ColaboradorDTO(Colaborador colaborador) {
		this(colaborador.getId(), colaborador.getNome(), colaborador.getFuncao(), colaborador.getCpf(), colaborador.getDataAdmissao(), colaborador.getRemuneracao(),
				colaborador.getNovaFuncao());
	}

	public Long id() {
		return id;
	}

	public String nome() {
		return nome;
	}

	public String funcao() {
		return funcao;
	}

	public String cpf() {
		return cpf;
	}

	public LocalDate dataAdmissao() {
		return dataAdmissao;
	}

	public BigDecimal remuneracao() {
		return remuneracao;
	}

	public String novaFuncao() {
		return novaFuncao;
	}

}
