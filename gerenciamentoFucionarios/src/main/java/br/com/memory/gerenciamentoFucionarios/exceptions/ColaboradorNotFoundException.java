package br.com.memory.gerenciamentoFucionarios.exceptions;

public class ColaboradorNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ColaboradorNotFoundException(Long id) {
		super("Colaborador com o ID: " + id + " nâo encontrado.");
	}
	

}
