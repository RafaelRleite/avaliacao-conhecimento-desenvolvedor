package br.com.memory.gerenciamentoFucionarios.exceptions;

public class SubordinadoNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SubordinadoNotFoundException(Long id) {
		super("Subordinado com o ID: " + id + " nâo encontrado.");
	}

}
