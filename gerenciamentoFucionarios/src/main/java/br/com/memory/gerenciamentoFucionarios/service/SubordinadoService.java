package br.com.memory.gerenciamentoFucionarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.memory.gerenciamentoFucionarios.dto.SubordinadoDTO;
import br.com.memory.gerenciamentoFucionarios.exceptions.SubordinadoNotFoundException;
import br.com.memory.gerenciamentoFucionarios.model.Gerente;
import br.com.memory.gerenciamentoFucionarios.model.Presidente;
import br.com.memory.gerenciamentoFucionarios.model.Subordinado;
import br.com.memory.gerenciamentoFucionarios.repository.SubordinadoRepository;

@Service
public class SubordinadoService extends ColaboradorService {

	@Autowired
	private SubordinadoRepository subordinadoRepository;

	public Subordinado salvarSubordinado(Subordinado subordinado) {
		return subordinadoRepository.save(subordinado);
	}

	public void deleteSubordinadoPorID(Long id) {
		buscarSubordinadoPorId(id);
		subordinadoRepository.deleteById(id);
	}

	public Subordinado alterarSubordinadoPorId(Long id, Subordinado subordinadoAtualizar) {
		Subordinado subordinadoBanco = buscarSubordinadoPorId(id);
		subordinadoBanco = (Subordinado) super.atualizarDadosColaborador(subordinadoBanco, subordinadoAtualizar);

		if (subordinadoAtualizar.getNovaFuncao() != null && subordinadoAtualizar.getNovaFuncao() != "") {
			if (subordinadoAtualizar.getNovaFuncao().equals("Gerente")) {
				super.transformarSubordinadoEmGerente(subordinadoBanco);
				return null;

			}
			if (subordinadoAtualizar.getNovaFuncao().equals("Presidente")) {
				super.transformarSubordinadoEmPresidente(subordinadoBanco);
				return null;
			}
		}

		return salvarSubordinado(subordinadoBanco);
	}

	public Page<SubordinadoDTO> buscarSubordinadosPaginado(Pageable paginacao) {
		Page<SubordinadoDTO> subordinados = subordinadoRepository.findAll(paginacao).map(SubordinadoDTO::new);
		return subordinados;
	}
	
	public List<SubordinadoDTO> buscarSubordinados() {
		 Sort sort = Sort.by(Sort.Direction.ASC, "id");
		List<SubordinadoDTO> subordinados = subordinadoRepository.findAll(sort).stream().map(SubordinadoDTO::new).toList();
		return subordinados;
	}

	public Subordinado buscarSubordinadoPorId(Long id) {
		return subordinadoRepository.findById(id).orElseThrow(() -> new SubordinadoNotFoundException(id));
	}

	public Gerente buscarGerenteSubordinadoPorId(Long id) {
		Subordinado subordinado = buscarSubordinadoPorId(id);
		return subordinado.getGerente();
	}

	public Presidente buscarPresidenteSubordinadoPorId(Long id) {
		Subordinado subordinado = buscarSubordinadoPorId(id);
		return subordinado.getPresidente();
	}
}
