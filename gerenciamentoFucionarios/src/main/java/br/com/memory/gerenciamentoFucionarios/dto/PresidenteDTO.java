package br.com.memory.gerenciamentoFucionarios.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.memory.gerenciamentoFucionarios.model.Presidente;

public record PresidenteDTO(Long id, String nome, String funcao, String cpf, LocalDate dataAdmissao, BigDecimal remuneracao, String novaFuncao) {
	public PresidenteDTO(Presidente presidente) {
		this(presidente.getId(), presidente.getNome(), presidente.getFuncao(), presidente.getCpf(), presidente.getDataAdmissao(), presidente.getRemuneracao(),
				presidente.getNovaFuncao());
	}

	public Long id() {
		return id;
	}

	public String nome() {
		return nome;
	}

	public String funcao() {
		return funcao;
	}

	public String cpf() {
		return cpf;
	}

	public LocalDate dataAdmissao() {
		return dataAdmissao;
	}

	public BigDecimal remuneracao() {
		return remuneracao;
	}

	public String novaFuncao() {
		return novaFuncao;
	}

}
