package br.com.memory.gerenciamentoFucionarios.model.conversor;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter
public class CPFConversor implements AttributeConverter<String, String> {

	@Override
	public String convertToDatabaseColumn(String valor) {
		if (valor != null) {
			String CPF = "[0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}";
			if (valor.matches(CPF)) {
				valor = valor.replaceAll("\\D", "");
			}
		}
		return valor;
	}

	@Override
    public String convertToEntityAttribute(String dbData) {
        while (dbData.length() > 11) {
        	dbData = "0" + dbData;
		}
        return dbData.substring(0, 3) + "." + dbData.substring(3, 6) + "." + dbData.substring(6, 9) + "-" + dbData.substring(9, 11);
    }
}
