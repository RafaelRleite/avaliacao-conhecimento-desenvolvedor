package br.com.memory.gerenciamentoFucionarios.exceptions;

public class PresidenteNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PresidenteNotFoundException(Long id) {
		super("Presidente com o ID: " + id + " nâo encontrado.");
	}
	
	

}
