package br.com.memory.gerenciamentoFucionarios.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "presidente")
@PrimaryKeyJoinColumn(name = "colaborador", foreignKey = @ForeignKey(name = "FK_colaborador_id"))
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Presidente extends Colaborador {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "presidente_seg_generator", sequenceName = "presidente_id_seg", allocationSize = 1)
	@GeneratedValue(generator = "presidente_seg_generator", strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToMany(mappedBy = "presidente", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Gerente> gerentes;
	
	@OneToMany(mappedBy = "presidente", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Subordinado> subordinados;

}
