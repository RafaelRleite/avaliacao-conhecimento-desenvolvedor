package br.com.memory.gerenciamentoFucionarios.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.memory.gerenciamentoFucionarios.model.conversor.CPFConversor;
import br.com.memory.gerenciamentoFucionarios.model.conversor.DataConversor;
import br.com.memory.gerenciamentoFucionarios.model.conversor.RemuneracaoConversor;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "colaborador", uniqueConstraints = @UniqueConstraint(name = "colaborador_ukey", columnNames = { "cpf" }))
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public abstract class Colaborador implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "colaborador_seg_generator", sequenceName = "colaborador_id_seg", allocationSize = 1)
	@GeneratedValue(generator = "colaborador_seg_generator", strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	@Column(nullable = false, unique = true)
	@Convert(converter = CPFConversor.class)
	private String cpf;

	@NotBlank
	@Column(nullable = false)
	private String nome;

	@Column(nullable = false)
	@NotNull
	@Convert(converter = DataConversor.class)
	private LocalDate dataAdmissao;

	@Column(nullable = false)
	@NotNull
	@Convert(converter = RemuneracaoConversor.class)
	private BigDecimal remuneracao;

	@Column(nullable = false)
	@NotNull
	private String funcao;
	
	@Column(nullable = false)
	@NotNull
	private String novaFuncao;
	
}
