package br.com.memory.gerenciamentoFucionarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.gerenciamentoFucionarios.dto.SubordinadoDTO;
import br.com.memory.gerenciamentoFucionarios.model.Gerente;
import br.com.memory.gerenciamentoFucionarios.model.Presidente;
import br.com.memory.gerenciamentoFucionarios.model.Subordinado;
import br.com.memory.gerenciamentoFucionarios.service.SubordinadoService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/")
public class SubordinadoController {

	@Autowired
	private SubordinadoService subordinadoService;

	@PostMapping("/salvar-subordinado")
	public ResponseEntity<Subordinado> salvarSubordinado(@RequestBody @Valid Subordinado subordinado) {
		return ResponseEntity.ok(subordinadoService.salvarSubordinado(subordinado));
	}

	@PutMapping("/alterar-subordinado/{id}")
	public ResponseEntity<Subordinado> alterarSubordinadoPorId(@PathVariable("id") Long id, @RequestBody Subordinado subordinado) {
		return ResponseEntity.ok(subordinadoService.alterarSubordinadoPorId(id, subordinado));
	}

	@DeleteMapping("/delete-subordinado/{id}")
	public ResponseEntity<Void> deleteSubordinadoPorID(@PathVariable("id") Long id) {
		subordinadoService.deleteSubordinadoPorID(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/subordinados")
	@ResponseStatus(HttpStatus.OK)
	public Page<SubordinadoDTO> buscarSubordinadosPaginado(@PageableDefault(page = 0, size = 10, sort = { "id" }) Pageable paginacao) {
		return subordinadoService.buscarSubordinadosPaginado(paginacao);
	}
	
	@GetMapping("/subornidadosAll")
	public ResponseEntity<List<SubordinadoDTO>> buscarSubordinados(){
		return ResponseEntity.ok(subordinadoService.buscarSubordinados());
	}

	@GetMapping("/subordinado/{id}")
	public ResponseEntity<Subordinado> buscarSubordinadoPorId(@PathVariable("id") Long id) {
		return ResponseEntity.ok(subordinadoService.buscarSubordinadoPorId(id));
	}

	@GetMapping("/{subordinadoId}/subordinado/gerente")
	public ResponseEntity<Gerente> buscarGerenteSubordinadoPorId(@PathVariable("subordinadoId") Long id) {
		return ResponseEntity.ok(subordinadoService.buscarGerenteSubordinadoPorId(id));
	}

	@GetMapping("/{subordinadoId}/subordinado/presidente")
	public ResponseEntity<Presidente> buscarPresidenteSubordinadoPorId(@PathVariable("subordinadoId") Long id) {
		return ResponseEntity.ok(subordinadoService.buscarPresidenteSubordinadoPorId(id));
	}

}
