package br.com.memory.gerenciamentoFucionarios.model.conversor;

import java.math.BigDecimal;
import java.math.RoundingMode;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter
public class RemuneracaoConversor implements AttributeConverter<BigDecimal, BigDecimal> {

    @Override
    public BigDecimal convertToDatabaseColumn(BigDecimal remuneracao) {
        if (remuneracao != null) {
            return remuneracao;
        }
        return null;
    }

    @Override
    public BigDecimal convertToEntityAttribute(BigDecimal remuneracao) {
        if (remuneracao != null) {
        	BigDecimal scaledValue = remuneracao.setScale(2, RoundingMode.HALF_UP);
            return scaledValue;
        }
        return null;
    }
}

