package br.com.memory.gerenciamentoFucionarios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GerenciamentoFucionariosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GerenciamentoFucionariosApplication.class, args);
	}

}
